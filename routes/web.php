<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/logout', 'LoginController@logout');

Route::group([
	"middleware" => ["login"]
], function () {
	Route::get('/login', 'LoginController@index')->name('login');
	Route::post('/login', 'LoginController@login');
});

Route::group([
	"middleware" => ['checkLogin']
], function () {
	Route::get('/', 'DashboardController@index')->name('homeAdmin');

	Route::post('/wilayah/propinsi', 'Part\WilayahController@propinsi');
	Route::post('/wilayah/kabupaten', 'Part\WilayahController@kabupaten');
	Route::post('/wilayah/kecamatan', 'Part\WilayahController@kecamatan');
	Route::post('cekKode', 'Part\CheckerController@cekKode')->name("cekKode");
	Route::post('getKode', 'Part\CheckerController@getKode')->name("getKode");

	Route::resource('/buku', 'BukuController');
	Route::resource('/jenis', 'JenisController');
	Route::resource('/bidang', 'BidangController');
	Route::resource('/penulis', 'PenulisController');
	Route::resource('/penerbit', 'PenerbitController');
	Route::resource('/propinsi', 'PropinsiController');
	Route::resource('/kabupaten', 'KabupatenController');
	Route::resource('/kecamatan', 'KecamatanController');
	Route::resource('/anggota', 'AnggotaController');
	Route::resource('/pinjam', 'PinjamController');
	Route::get('/pinjam/kembali/{id}', 'PinjamController@kembali')->name("pinjam.kembali");
	Route::post('/pinjam/kembali', 'PinjamController@kembali_proses')->name("pinjam.kembali_proses");
	Route::get('/pinjam/denda/{id}', 'PinjamController@denda')->name("pinjam.denda");
	Route::post('/pinjam/denda', 'PinjamController@denda_proses')->name("pinjam.denda_proses");
	Route::get('/pinjam/rusak/{id}', 'PinjamController@rusak')->name("pinjam.rusak");
	Route::post('/pinjam/rusak', 'PinjamController@rusak_proses')->name("pinjam.rusak_proses");
	Route::get('/pinjam/hilang/{id}', 'PinjamController@hilang')->name("pinjam.hilang");
	Route::post('/pinjam/hilang', 'PinjamController@hilang_proses')->name("pinjam.hilang_proses");
});

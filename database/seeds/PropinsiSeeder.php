<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class PropinsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$data = $this->data();
		DB::table("propinsi")->insert($data);
	}

	protected function data () {
		$faker = Faker::create();
		return [
			["kode_propinsi" => 11, "nama_propinsi" => "ACEH", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 12, "nama_propinsi" => "SUMATERA UTARA", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 13, "nama_propinsi" => "SUMATERA BARAT", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 14, "nama_propinsi" => "RIAU", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 15, "nama_propinsi" => "JAMBI", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 16, "nama_propinsi" => "SUMATERA SELATAN", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 17, "nama_propinsi" => "BENGKULU", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 18, "nama_propinsi" => "LAMPUNG", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 19, "nama_propinsi" => "KEPULAUAN BANGKA BELITUNG", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 21, "nama_propinsi" => "KEPULAUAN RIAU", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 31, "nama_propinsi" => "DKI JAKARTA", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 32, "nama_propinsi" => "JAWA BARAT", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 33, "nama_propinsi" => "JAWA TENGAH", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 34, "nama_propinsi" => "DI YOGYAKARTA", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 35, "nama_propinsi" => "JAWA TIMUR", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 36, "nama_propinsi" => "BANTEN", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 51, "nama_propinsi" => "BALI", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 52, "nama_propinsi" => "NUSA TENGGARA BARAT", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 53, "nama_propinsi" => "NUSA TENGGARA TIMUR", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 61, "nama_propinsi" => "KALIMANTAN BARAT", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 62, "nama_propinsi" => "KALIMANTAN TENGAH", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 63, "nama_propinsi" => "KALIMANTAN SELATAN", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 64, "nama_propinsi" => "KALIMANTAN TIMUR", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 65, "nama_propinsi" => "KALIMANTAN UTARA", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 71, "nama_propinsi" => "SULAWESI UTARA", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 72, "nama_propinsi" => "SULAWESI TENGAH", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 73, "nama_propinsi" => "SULAWESI SELATAN", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 74, "nama_propinsi" => "SULAWESI TENGGARA", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 75, "nama_propinsi" => "GORONTALO", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 76, "nama_propinsi" => "SULAWESI BARAT", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 81, "nama_propinsi" => "MALUKU", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 82, "nama_propinsi" => "MALUKU UTARA", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 91, "nama_propinsi" => "PAPUA BARAT", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
			["kode_propinsi" => 94, "nama_propinsi" => "PAPUA", "created_at" => $faker->dateTime, "updated_at" => $faker->dateTime],
		];
	}
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
			$table->increments('id');
            $table->string('kode_buku', 10)->unique();
            $table->string('kode_jenis', 10);
            $table->string('kode_bidang', 10);
            $table->string('kode_penulis', 10);
            $table->string('kode_penerbit', 10);
            $table->string('judul_buku');
            $table->integer('jumlah');
            $table->string('bahasa');
            $table->string('isbn');
            $table->year('tahun');
            $table->integer('jumlah_halaman');
            $table->integer('edisi');
            $table->integer('cetakan_ke');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}

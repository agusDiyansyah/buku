<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnggota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_anggota', 20)->unique();
            $table->string('kode_kecamatan', 20);
            $table->string('nama_anggota');
            $table->text('alamat');
            $table->string('telp');
            $table->string('email');
            $table->date('tanggal_mulai_anggota');
            $table->enum('jenis_anggota', ['umum', 'mahasiswa']);
            $table->enum('status_anggota', [1, 0]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota');
    }
}

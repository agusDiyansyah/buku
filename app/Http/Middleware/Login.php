<?php

namespace App\Http\Middleware;

use Closure;

class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (
			$request->session()->has('logged')
			AND $request->session()->get('logged')
		) {
			return redirect()->route('homeAdmin');
		}

        return $next($request);
    }
}

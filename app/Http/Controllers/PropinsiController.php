<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Propinsi;
use Collective\Html\FormFacade as Form;

class PropinsiController extends Controller
{
    protected $page = "admin.page.propinsi";
    protected $title = "Propinsi";
    protected $module = "propinsi";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r) {
		$perpage = 20;
		$page = empty($r->page) ? 1 : $r->page;
		$page = (($page*$perpage)-$perpage) + 1;

        $data = array(
            "title" => "Data $this->title",
            "form" => url("$this->module/create"),
			"module" => $this->module,
			"no" => $page,
            "data" => Propinsi::orderBy('created_at', 'desc')->paginate($perpage)
        );

        return view("$this->page.data", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
		$data = $this->formData();
        return view("$this->page.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->rules($request);

		$db = new Propinsi;
		$this->proses($db, $request);

        return redirect($this->module);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array(
            "title" => "Detail Data $this->title",
            "data" => Propinsi::find($id)
        );
        return view("$this->page.detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$data = $this->formData($id);
        return view("$this->page.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules($request);

		$db = Propinsi::find($id);
		$this->proses($db, $request);

        return redirect($this->module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $db = Propinsi::find($id);
        $db->delete();
        return redirect($this->module);
	}

	protected function proses ($db, $r) {
        $db->kode_propinsi = $r->kode_propinsi;
		$db->nama_propinsi = $r->nama_propinsi;

        $db->save();
	}

	protected function formData ($id = 0) {
		if ($id > 0) {
			$title = "Ubah Data $this->title";
			$aksi = url("$this->module/$id");
			$method = "PUT";

			$item = Propinsi::find($id);
		} else {
			$title = "Tambah Data $this->title";
			$method = "POST";
			$aksi = url($this->module);
		}

		return array(
            "title" => $title,
			"back" => url($this->module),

			"form" => [
				"open" => Form::open([
					"class" => "form",
					"url" => $aksi,
					"method" => $method,
					"data-id" => $id,
					// "enctype" => "multipart/form-data",
					// "target" => "_blank",
				]),
				"close" => Form::close(),

				"kode_propinsi" => Form::number("kode_propinsi",
					(empty($item->kode_propinsi)
						? old("kode_propinsi")
						: $item->kode_propinsi),
					["class" => "form-control kode_propinsi"]
				),
				"nama_propinsi" => Form::text("nama_propinsi",
					(empty($item->nama_propinsi)
						? old("nama_propinsi")
						: $item->nama_propinsi),
					["class" => "form-control nama_propinsi"]
				),
			],
		);
	}

    protected function rules ($r) {
        $r->validate(
            array(
                "kode_propinsi" => 'required',
            ),
            array(
                "kode_propinsi.required" => "Kode tidak boleh kosong",
                // "kode_bidang.unique" => "Kode jenis telah digunakan",
                // "kode_bidang.min" => "Harap menggunakan minimal 5 karakter",
                // "kode_bidang.max" => "Harap menggunakan maksimal 10 karakter",
                // "nama_bidang.required" => "Nama jenis tidak boleh kosong",
            )
        );
    }
}

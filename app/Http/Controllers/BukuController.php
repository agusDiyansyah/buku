<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Buku;
use Collective\Html\FormFacade as Form;

class BukuController extends Controller
{
    protected $page = "admin.page.buku";
    protected $title = "Buku";
    protected $module = "buku";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r) {
		$sql = Buku::orderBy('created_at', 'desc');

		if (!empty($r->judul_buku)) {
			$sql->where("judul_buku", "like", "%$r->judul_buku%");
		}

        $data = array(
            "title" => "Data $this->title",
            "form" => url("$this->module/create"),
            "module" => $this->module,
            "data" => $sql->paginate(50)
        );

        return view("$this->page.data", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
		$data = $this->formData();
        return view("$this->page.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->rules($request);

		$db = new Buku;
		$db->status = "tersimpan";
		$this->proses($db, $request);

        return redirect($this->module);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array(
            "title" => "Detail Data $this->title",
            "data" => Buku::find($id)
        );
        return view("$this->page.detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$data = $this->formData($id);
        return view("$this->page.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules($request);

		$db = Buku::find($id);
		$this->proses($db, $request);

        return redirect($this->module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $db = Buku::find($id);
        $db->delete();
        return redirect($this->module);
	}

	protected function proses ($db, $r) {
        $db->kode_buku = $r->kode_buku;
        $db->kode_jenis = $r->kode_jenis;
        $db->kode_bidang = $r->kode_bidang;
        $db->kode_penulis = $r->kode_penulis;
		$db->kode_penerbit = $r->kode_penerbit;
		$db->judul_buku = $r->judul_buku;
		$db->jumlah = $r->jumlah;
		$db->bahasa = $r->bahasa;
		$db->isbn = $r->isbn;
		$db->tahun = $r->tahun;
		$db->jumlah_halaman = $r->jumlah_halaman;
		$db->edisi = $r->edisi;
		$db->cetakan_ke = $r->cetakan_ke;
		$db->status = $r->status;

        $db->save();
	}

	protected function formData ($id = 0) {
		if ($id > 0) {
			$title = "Ubah Data $this->title";
			$aksi = url("$this->module/$id");
			$method = "PUT";

			$item = Buku::find($id);
			$kode_buku = [$item->kode_buku => $item->kode_buku];

			$sql = DB::table("buku as b")
				->where("b.id", "=", $id)
				->join("jenis as j", "b.kode_jenis", "j.kode_jenis")
				->join("bidang as d", "b.kode_bidang", "d.kode_bidang")
				->join("penerbit as n", "b.kode_penerbit", "n.kode_penerbit")
				->join("penulis as s", "b.kode_penulis", "s.kode_penulis")
				->select(
					"nama_penulis_utama",
					"b.kode_penulis",
					"nama_penerbit",
					"b.kode_penerbit",
					"nama_jenis",
					"b.kode_jenis",
					"nama_bidang",
					"b.kode_bidang",
				)
				->get();

			if (count($sql) > 0) {
				$sql = $sql->first();
				$kode_penulis = [$sql->kode_penulis => "$sql->kode_penulis - $sql->nama_penulis_utama"];
				$kode_penerbit = [$sql->kode_penerbit => "$sql->kode_penerbit - $sql->nama_penerbit"];
				$kode_jenis = [$sql->kode_jenis => "$sql->kode_jenis - $sql->nama_jenis"];
				$kode_bidang = [$sql->kode_bidang => "$sql->kode_bidang - $sql->nama_bidang"];
			}

		} else {
			$title = "Tambah Data $this->title";
			$method = "POST";
			$aksi = url($this->module);
		}

		return array(
            "title" => $title,
			"back" => url($this->module),

			"form" => [
				"open" => Form::open([
					"class" => "form",
					"url" => $aksi,
					"method" => $method,
					"data-id" => $id,
					// "enctype" => "multipart/form-data",
					// "target" => "_blank",
				]),
				"close" => Form::close(),

				"kode_buku" => Form::select("kode_buku",
					(empty($kode_buku) ? [] : $kode_buku),
					null,
					["class" => "form-control kode_buku"]
				),
				"kode_jenis" => Form::select("kode_jenis",
					(empty($kode_jenis) ? [] : $kode_jenis),
					null,
					["class" => "form-control kode_jenis s2"]
				),
				"kode_bidang" => Form::select("kode_bidang",
					(empty($kode_bidang) ? [] : $kode_bidang),
					null,
					["class" => "form-control kode_bidang s2"]
				),
				"kode_penulis" => Form::select("kode_penulis",
					(empty($kode_penulis) ? [] : $kode_penulis),
					null,
					["class" => "form-control kode_penulis s2"]
				),
				"kode_penerbit" => Form::select("kode_penerbit",
					(empty($kode_penerbit) ? [] : $kode_penerbit),
					null,
					["class" => "form-control kode_penerbit s2"]
				),
				"judul_buku" => Form::text("judul_buku",
					(empty($item->judul_buku)
						? old("judul_buku")
						: $item->judul_buku),
					["class" => "form-control judul_buku"]
				),
				"jumlah" => Form::number("jumlah",
					(empty($item->jumlah)
						? old("jumlah")
						: $item->jumlah),
					["class" => "form-control jumlah"]
				),
				"bahasa" => Form::text("bahasa",
					(empty($item->bahasa)
						? old("bahasa")
						: $item->bahasa),
					["class" => "form-control bahasa"]
				),
				"isbn" => Form::text("isbn",
					(empty($item->isbn)
						? old("isbn")
						: $item->isbn),
					["class" => "form-control isbn"]
				),
				"tahun" => Form::text("tahun",
					(empty($item->tahun)
						? old("tahun")
						: $item->tahun),
					["class" => "form-control tahun", "readonly" => true]
				),
				"jumlah_halaman" => Form::text("jumlah_halaman",
					(empty($item->jumlah_halaman)
						? old("jumlah_halaman")
						: $item->jumlah_halaman),
					["class" => "form-control jumlah_halaman"]
				),
				"edisi" => Form::text("edisi",
					(empty($item->edisi)
						? old("edisi")
						: $item->edisi),
					["class" => "form-control edisi"]
				),
				"cetakan_ke" => Form::text("cetakan_ke",
					(empty($item->cetakan_ke)
						? old("cetakan_ke")
						: $item->cetakan_ke),
					["class" => "form-control cetakan_ke"]
				),
			],
		);
	}

    protected function rules ($r) {
        $r->validate(
            array(
                "kode_penerbit" => 'required',
            ),
            array(
                "kode_penerbit.required" => "Kode penerbit tidak boleh kosong",
                // "kode_bidang.unique" => "Kode jenis telah digunakan",
                // "kode_bidang.min" => "Harap menggunakan minimal 5 karakter",
                // "kode_bidang.max" => "Harap menggunakan maksimal 10 karakter",
                // "nama_bidang.required" => "Nama jenis tidak boleh kosong",
            )
        );
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Anggota;
use Collective\Html\FormFacade as Form;

class AnggotaController extends Controller
{
    protected $page = "admin.page.anggota";
    protected $title = "Anggota";
    protected $module = "anggota";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r) {
		$perpage = 20;
		$page = empty($r->page) ? 1 : $r->page;
		$page = (($page*$perpage)-$perpage) + 1;

        $data = array(
            "title" => "Data $this->title",
            "form" => url("$this->module/create"),
			"module" => $this->module,
			"no" => $page,
            "data" => Anggota::orderBy('id', 'desc')->paginate(20)
        );

        return view("$this->page.data", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
		$data = $this->formData();
        return view("$this->page.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->rules($request);

		$db = new Anggota;
		$this->proses($db, $request);

        return redirect($this->module);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array(
            "title" => "Detail Data $this->title",
            "data" => Anggota::find($id)
        );
        return view("$this->page.detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$data = $this->formData($id);
        return view("$this->page.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules($request);

		$db = Anggota::find($id);
		$this->proses($db, $request);

        return redirect($this->module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $db = Anggota::find($id);
        $db->delete();
        return redirect($this->module);
	}

	protected function proses ($db, $r) {
		if (!empty($r->kode_kecamatan)) {
			$db->kode_kecamatan = $r->kode_kecamatan;
		}
        $db->kode_anggota = $r->kode_anggota;
		$db->nama_anggota = $r->nama_anggota;
		$db->alamat = $r->alamat;
		$db->telp = $r->telp;
		$db->email = $r->email;
		$db->tanggal_mulai_anggota = $r->tanggal_mulai_anggota;
		$db->jenis_anggota = $r->jenis_anggota;
		$db->status_anggota = $r->status_anggota;

        $db->save();
	}

	protected function formData ($id = 0) {
		if ($id > 0) {
			$title = "Ubah Data $this->title";
			$aksi = url("$this->module/$id");
			$method = "PUT";

			$item = Anggota::find($id);
			$kode_anggota = [$item->kode_anggota => $item->kode_anggota];

			$sql = DB::table("anggota as p")
			->join("kecamatan as kec", "p.kode_kecamatan", "kec.kode_kecamatan")
			->join("kabupaten as kab", "kec.kode_kabupaten", "kab.kode_kabupaten")
			->join("propinsi as pro", "kab.kode_propinsi", "pro.kode_propinsi")
			->select(
				"kec.kode_kecamatan",
				"kab.kode_kabupaten",
				"pro.kode_propinsi",
				"kec.nama_kecamatan",
				"kab.nama_kabupaten",
				"pro.nama_propinsi",
			)
			->where("p.id", "=", $id)
			->get();

			if (count($sql) > 0) {
				$sql = $sql->first();
				$kode_propinsi = $sql->kode_propinsi;
				$kode_kabupaten = $sql->kode_kabupaten;
				$kode_kecamatan = $sql->kode_kecamatan;
				$nama_kecamatan = $sql->nama_kecamatan;
				$nama_kabupaten = $sql->nama_kabupaten;
				$nama_propinsi = $sql->nama_propinsi;
			}

		} else {
			$title = "Tambah Data $this->title";
			$method = "POST";
			$aksi = url($this->module);
		}

		return array(
            "title" => $title,
			"back" => url($this->module),

			"nama_kecamatan" => (empty($nama_kecamatan) ? "" : $nama_kecamatan),
			"nama_kabupaten" => (empty($nama_kabupaten) ? "" : $nama_kabupaten),
			"nama_propinsi" => (empty($nama_propinsi) ? "" : $nama_propinsi),

			"form" => [
				"open" => Form::open([
					"class" => "form",
					"url" => $aksi,
					"method" => $method,
					"data-id" => $id,
					// "enctype" => "multipart/form-data",
					// "target" => "_blank",
				]),
				"close" => Form::close(),

				"kode_propinsi" => Form::select("kode_propinsi",
					[],
					null,
					["class" => "form-control kode_propinsi"]
				),
				"kode_kabupaten" => Form::select("kode_kabupaten",
					[],
					null,
					["class" => "form-control kode_kabupaten"]
				),
				"kode_kecamatan" => Form::select("kode_kecamatan",
					[],
					null,
					["class" => "form-control kode_kecamatan"]
				),

				"kode_anggota" => Form::select("kode_anggota",
					(empty($kode_anggota) ? [] : $kode_anggota),
					null,
					["class" => "form-control kode_anggota"]
				),
				"nama_anggota" => Form::text("nama_anggota",
					(empty($item->nama_anggota)
						? old("nama_anggota")
						: $item->nama_anggota),
					["class" => "form-control nama_anggota"]
				),
				"alamat" => Form::text("alamat",
					(empty($item->alamat)
						? old("alamat")
						: $item->alamat),
					["class" => "form-control alamat"]
				),
				"telp" => Form::text("telp",
					(empty($item->telp)
						? old("telp")
						: $item->telp),
					["class" => "form-control telp"]
				),
				"email" => Form::email("email",
					(empty($item->email)
						? old("email")
						: $item->email),
					["class" => "form-control email"]
				),
				"tanggal_mulai_anggota" => Form::text("tanggal_mulai_anggota",
					(empty($item->tanggal_mulai_anggota)
						? old("tanggal_mulai_anggota")
						: $item->tanggal_mulai_anggota),
					["class" => "form-control tanggal_mulai_anggota date"]
				),
				"jenis_anggota" => Form::select("jenis_anggota",
					["" => "", "mahasiswa" => "Mahasiswa", "umum" => "Umum"],
					(empty($item->jenis_anggota) ? null : $item->jenis_anggota),
					["class" => "form-control jenis_anggota s2"]
				),
				"status_anggota" => Form::select("status_anggota",
					["" => "", "0" => "Tidak Aktif", "1" => "Aktif"],
					(empty($item->status_anggota) ? null : $item->status_anggota),
					["class" => "form-control status_anggota s2"]
				),
			],
		);
	}

    protected function rules ($r) {
        $r->validate(
            array(
                "kode_anggota" => 'required',
            ),
            array(
                "kode_anggota.required" => "Kode anggota tidak boleh kosong",
                // "kode_bidang.unique" => "Kode jenis telah digunakan",
                // "kode_bidang.min" => "Harap menggunakan minimal 5 karakter",
                // "kode_bidang.max" => "Harap menggunakan maksimal 10 karakter",
                // "nama_bidang.required" => "Nama jenis tidak boleh kosong",
            )
        );
    }
}

<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Bidang;

class BidangController extends Controller
{
    protected $page = "admin.page.bidang";
    protected $title = "Bidang";
    protected $module = "bidang";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r) {
		$page = empty($r->page) ? 1 : $r->page;
		$page = (($page*10)-10) + 1;

        $data = array(
            "title" => "Data $this->title",
            "form" => url("$this->module/create"),
			"module" => $this->module,
			"no" => $page,
            "data" => Bidang::orderBy('created_at', 'desc')->paginate(10)
        );

        return view("$this->page.data", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array(
            "title" => "Tambah Data $this->title",
            "method" => "POST",
            "aksi" => url($this->module),
            "back" => url($this->module),
            "data" => array(
                "kode_bidang" => old('kode_bidang'),
                "nama_bidang" => old('nama_bidang'),
            )
        );
        return view("$this->page.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->rules($request);

        $db = new Bidang;

        $db->kode_bidang = $request->kode_bidang;
        $db->nama_bidang = $request->nama_bidang;

        $db->save();
        return redirect($this->module);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array(
            "title" => "Detail Data $this->title",
            "data" => Bidang::find($id)
        );
        return view("$this->page.detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$item = Bidang::find($id);
        $data = array(
            "title" => "Ubah Data $this->title",
            "aksi" => url("$this->module/$item->id"),
			"method" => "PUT",
			"back" => url("aksi"),
            "data" => array(
                "kode_bidang" => $item->kode_bidang,
                "nama_bidang" => $item->nama_bidang,
            )
        );
        return view("$this->page.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules($request);

        $db = Bidang::find($id);

        $db->kode_bidang = $request->kode_bidang;
        $db->nama_bidang = $request->nama_bidang;

        $db->save();

        return redirect($this->module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $db = Bidang::find($id);
        $db->delete();
        return redirect($this->module);
    }

    protected function rules ($r) {
        $r->validate(
            array(
                "kode_bidang" => 'required|max:10|min:5',
                "nama_bidang" => 'required',
            ),
            array(
                "kode_bidang.required" => "Kode jenis tidak boleh kosong",
                // "kode_bidang.unique" => "Kode jenis telah digunakan",
                "kode_bidang.min" => "Harap menggunakan minimal 5 karakter",
                "kode_bidang.max" => "Harap menggunakan maksimal 10 karakter",
                "nama_bidang.required" => "Nama jenis tidak boleh kosong",
            )
        );
    }
}

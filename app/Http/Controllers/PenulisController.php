<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Penulis;
use Collective\Html\FormFacade as Form;

class PenulisController extends Controller
{
    protected $page = "admin.page.penulis";
    protected $title = "Penulis";
    protected $module = "penulis";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array(
            "title" => "Data $this->title",
            "form" => url("$this->module/create"),
            "module" => $this->module,
            "data" => Penulis::orderBy('created_at', 'desc')->paginate(50)
        );

        return view("$this->page.data", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = $this->formData();
        return view("$this->page.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->rules($request);

		$db = new Penulis;
		$this->proses($db, $request);

        return redirect($this->module);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array(
            "title" => "Detail Data $this->title",
            "data" => Penulis::find($id)
        );
        return view("$this->page.detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = $this->formData($id);
        return view("$this->page.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules($request);

		$db = Penulis::find($id);
		$this->proses($db, $request);

        return redirect($this->module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $db = Penulis::find($id);
        $db->delete();
        return redirect($this->module);
	}

	protected function proses ($db, $r) {
		if (!empty($r->kode_kecamatan)) {
			$db->kode_kecamatan = $r->kode_kecamatan;
		}
        $db->kode_penulis = $r->kode_penulis;
		$db->nama_penulis_utama = $r->nama_penulis_utama;
		$db->alamat = $r->alamat;
		$db->telp = $r->telp;
		$db->email = $r->email;

        $db->save();
	}

	protected function formData ($id = 0) {
		if ($id > 0) {
			$title = "Ubah Data $this->title";
			$aksi = url("$this->module/$id");
			$method = "PUT";

			$item = Penulis::find($id);
			$kode_penulis = [$item->kode_penulis => $item->kode_penulis];

			$sql = DB::table("penulis as p")
			->join("kecamatan as kec", "p.kode_kecamatan", "kec.kode_kecamatan")
			->join("kabupaten as kab", "kec.kode_kabupaten", "kab.kode_kabupaten")
			->join("propinsi as pro", "kab.kode_propinsi", "pro.kode_propinsi")
			->select(
				"kec.kode_kecamatan",
				"kab.kode_kabupaten",
				"pro.kode_propinsi",
				"kec.nama_kecamatan",
				"kab.nama_kabupaten",
				"pro.nama_propinsi",
			)
			->where("p.id", "=", $id)
			->first();

			$kode_propinsi = $sql->kode_propinsi;
			$kode_kabupaten = $sql->kode_kabupaten;
			$kode_kecamatan = $sql->kode_kecamatan;
			$nama_kecamatan = $sql->nama_kecamatan;
			$nama_kabupaten = $sql->nama_kabupaten;
			$nama_propinsi = $sql->nama_propinsi;

		} else {
			$title = "Tambah Data $this->title";
			$method = "POST";
			$aksi = url($this->module);
		}

		return array(
            "title" => $title,
			"back" => url($this->module),

			"nama_kecamatan" => (empty($nama_kecamatan) ? "" : $nama_kecamatan),
			"nama_kabupaten" => (empty($nama_kabupaten) ? "" : $nama_kabupaten),
			"nama_propinsi" => (empty($nama_propinsi) ? "" : $nama_propinsi),

			"form" => [
				"open" => Form::open([
					"class" => "form",
					"url" =>$aksi,
					"method" => $method,
					"data-id" => $id,
					// "enctype" => "multipart/form-data",
					// "target" => "_blank",
				]),
				"close" => Form::close(),

				"kode_propinsi" => Form::select("kode_propinsi",
					[],
					null,
					["class" => "form-control kode_propinsi"]
				),
				"kode_kabupaten" => Form::select("kode_kabupaten",
					[],
					null,
					["class" => "form-control kode_kabupaten"]
				),
				"kode_kecamatan" => Form::select("kode_kecamatan",
					[],
					null,
					["class" => "form-control kode_kecamatan"]
				),

				"kode_penulis" => Form::select("kode_penulis",
					(empty($kode_penulis) ? [] : $kode_penulis),
					null,
					["class" => "form-control kode_penulis"]
				),
				"nama_penulis_utama" => Form::text("nama_penulis_utama",
					(empty($item->nama_penulis_utama)
						? old("nama_penulis_utama")
						: $item->nama_penulis_utama),
					["class" => "form-control nama_penulis_utama"]
				),
				"alamat" => Form::text("alamat",
					(empty($item->alamat)
						? old("alamat")
						: $item->alamat),
					["class" => "form-control alamat"]
				),
				"telp" => Form::text("telp",
					(empty($item->telp)
						? old("telp")
						: $item->telp),
					["class" => "form-control telp"]
				),
				"email" => Form::email("email",
					(empty($item->email)
						? old("email")
						: $item->email),
					["class" => "form-control email"]
				),
			],

            // "data" => array(
            //     "kode_penulis" => old('kode_penulis'),
            //     "kode_propinsi" => old('kode_propinsi'),
            //     "kode_kabupaten" => old('kode_kabupaten'),
            //     "kode_kecamatan" => old('kode_kecamatan'),
            //     "nama_penulis_utama" => old('nama_penulis_utama'),
            //     "alamat" => old('alamat'),
            //     "telp" => old('telp'),
            //     "email" => old('email'),
            // )
		);
	}

    protected function rules ($r) {
        $r->validate(
            array(
                "kode_penulis" => 'required',
            ),
            array(
                "kode_penulis.required" => "Kode penulis tidak boleh kosong",
                // "kode_bidang.unique" => "Kode jenis telah digunakan",
                // "kode_bidang.min" => "Harap menggunakan minimal 5 karakter",
                // "kode_bidang.max" => "Harap menggunakan maksimal 10 karakter",
                // "nama_bidang.required" => "Nama jenis tidak boleh kosong",
            )
        );
	}
}

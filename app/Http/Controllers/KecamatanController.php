<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Kecamatan;
use Collective\Html\FormFacade as Form;

class KecamatanController extends Controller
{
    protected $page = "admin.page.kecamatan";
    protected $title = "Kecamatan";
    protected $module = "kecamatan";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r) {
		$perpage = 50;
		$page = empty($r->page) ? 1 : $r->page;
		$page = (($page*$perpage)-$perpage) + 1;

        $data = array(
            "title" => "Data $this->title",
            "form" => url("$this->module/create"),
			"module" => $this->module,
			"no" => $page,
			"data" => DB::table("kecamatan as kec")
				->join("kabupaten as kab", "kec.kode_kabupaten", "kab.kode_kabupaten")
				->join("propinsi as pro", "kab.kode_propinsi", "pro.kode_propinsi")
				->select(
					"kec.id",
					"kode_kecamatan",
					"nama_kecamatan",
					"nama_kabupaten",
					"nama_propinsi",
				)
				->orderBy("kec.id", "desc")
				->paginate($perpage)
        );

        return view("$this->page.data", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
		$data = $this->formData();
        return view("$this->page.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->rules($request);

		$db = new Kecamatan;
		$this->proses($db, $request);

        return redirect($this->module);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array(
            "title" => "Detail Data $this->title",
            "data" => Kecamatan::find($id)
        );
        return view("$this->page.detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$data = $this->formData($id);
        return view("$this->page.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules($request);

		$db = Kecamatan::find($id);
		$this->proses($db, $request);

        return redirect($this->module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $db = Kecamatan::find($id);
        $db->delete();
        return redirect($this->module);
	}

	protected function proses ($db, $r) {
		if (!empty($r->kode_kabupaten)) {
			$db->kode_kabupaten = $r->kode_kabupaten;
		}
        $db->kode_kecamatan = $r->kode_kecamatan;
		$db->nama_kecamatan = $r->nama_kecamatan;
        $db->save();
	}

	protected function formData ($id = 0) {
		if ($id > 0) {
			$title = "Ubah Data $this->title";
			$aksi = url("$this->module/$id");
			$method = "PUT";

			$item = Kecamatan::find($id);
			$kode_kecamatan = [$item->kode_kecamatan => $item->kode_kecamatan];

			$sql = DB::table("kecamatan as kec")
			->join("kabupaten as kab", "kec.kode_kabupaten", "kab.kode_kabupaten")
			->join("propinsi as pro", "kab.kode_propinsi", "pro.kode_propinsi")
			->select(
				"kab.kode_kabupaten",
				"kab.nama_kabupaten",
				"pro.kode_propinsi",
				"pro.nama_propinsi",
			)
			->where("kec.id", "=", $id)
			->first();

			$nama_kabupaten = $sql->nama_kabupaten;
			$kode_kabupaten = $sql->kode_kabupaten;
			$nama_propinsi = $sql->nama_propinsi;
			$kode_propinsi = $sql->kode_propinsi;

		} else {
			$title = "Tambah Data $this->title";
			$method = "POST";
			$aksi = url($this->module);
		}

		return array(
            "title" => $title,
			"back" => url($this->module),

			"nama_propinsi" => (empty($nama_propinsi) ? "" : $nama_propinsi),
			"nama_kabupaten" => (empty($nama_kabupaten) ? "" : $nama_kabupaten),

			"form" => [
				"open" => Form::open([
					"class" => "form",
					"url" => $aksi,
					"method" => $method,
					"data-id" => $id,
					// "enctype" => "multipart/form-data",
					// "target" => "_blank",
				]),
				"close" => Form::close(),

				"kode_propinsi" => Form::select("kode_propinsi",
					[],
					null,
					["class" => "form-control kode_propinsi"]
				),
				"kode_kabupaten" => Form::select("kode_kabupaten",
					[],
					null,
					["class" => "form-control kode_kabupaten"]
				),

				"kode_kecamatan" => Form::select("kode_kecamatan",
					(empty($kode_kecamatan) ? [] : $kode_kecamatan),
					null,
					["class" => "form-control kode_kecamatan"]
				),
				"nama_kecamatan" => Form::text("nama_kecamatan",
					(empty($item->nama_kecamatan)
						? old("nama_kecamatan")
						: $item->nama_kecamatan),
					["class" => "form-control nama_kecamatan"]
				),
			],
		);
	}

    protected function rules ($r) {
        $r->validate(
            array(
                "kode_kecamatan" => 'required',
            ),
            array(
                "kode_kecamatan.required" => "Kode kecamatan tidak boleh kosong",
                // "kode_bidang.unique" => "Kode jenis telah digunakan",
                // "kode_bidang.min" => "Harap menggunakan minimal 5 karakter",
                // "kode_bidang.max" => "Harap menggunakan maksimal 10 karakter",
                // "nama_bidang.required" => "Nama jenis tidak boleh kosong",
            )
        );
    }
}

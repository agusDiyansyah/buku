<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Pinjam;
use App\Denda;
use App\Rusak;
use App\Hilang;
use Collective\Html\FormFacade as Form;

class PinjamController extends Controller
{
    protected $page = "admin.page.pinjam";
    protected $title = "Pinjam";
    protected $module = "pinjam";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r) {
		$perpage = 20;
		$page = empty($r->page) ? 1 : $r->page;
		$page = (($page*$perpage)-$perpage) + 1;

        $data = array(
            "title" => "Data $this->title",
            "form" => url("$this->module/create"),
			"module" => $this->module,
			"no" => $page,
			"data" => DB::table("pinjam as p")
				->join("buku as b", "p.kode_buku", "b.kode_buku")
				->join("anggota as a", "p.kode_anggota", "a.kode_anggota")
				->select(
					"b.status",
					"nama_anggota",
					"judul_buku as nama_buku",
					"p.*"
				)
				->paginate($perpage)
        );

        return view("$this->page.data", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
		$data = $this->formData();
        return view("$this->page.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->rules($request);

		$db = new Pinjam;
		$this->proses($db, $request);

        return redirect($this->module);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array(
            "title" => "Detail Data $this->title",
            "data" => Pinjam::find($id)
        );
        return view("$this->page.detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$data = $this->formData($id);
        return view("$this->page.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules($request);

		$db = Pinjam::find($id);
		$this->proses($db, $request, $id);

        return redirect($this->module);
	}

	public function kembali (Request $r) {
		$id = $r->id;
		$sql = DB::table("pinjam")->where("id", $id)->get();
		if (count($sql) > 0) {
			$data = array(
				"title" => "Mengembalikan Pinjaman Buku",
				"back" => url($this->module),

				"form" => [
					"open" => Form::open([
						"class" => "form",
						"url" => url("pinjam/kembali"),
						"method" => "POST",
						"data-id" => $id,
						// "enctype" => "multipart/form-data",
						// "target" => "_blank",
					]),
					"close" => Form::close(),

					"id" => Form::text("id",
						$id,
						["class" => "form-control id date d-none"]
					),
					"tanggal_kembali" => Form::text("tanggal_kembali",
						(empty($item->tanggal_kembali)
							? old("tanggal_kembali")
							: $item->tanggal_kembali),
						["class" => "form-control tanggal_kembali date"]
					),
				],
			);
			return view("$this->page.kembali", $data);
		}
	}

	public function kembali_proses (Request $r) {
		$id = $r->id;
		$sql = DB::table("pinjam")->where("id", $id)->get();
		if (count($sql) > 0) {
			$db = Pinjam::find($id);

			$db->tanggal_kembali = $r->tanggal_kembali;

			$db->save();

			$sql = DB::table("pinjam")->where("id", $id)->select("kode_buku")->first();
			DB::table("buku")->where("kode_buku", $sql->kode_buku)->update(["status" => "tersimpan"]);

			return redirect($this->module);
		}
	}

	public function denda (Request $r) {
		$id = $r->id;
		$sql = DB::table("pinjam")->where("id", $id)->get();
		if (count($sql) > 0) {
			$data = array(
				"title" => "Denda Pinjaman Buku",
				"back" => url($this->module),

				"form" => [
					"open" => Form::open([
						"class" => "form",
						"url" => url("pinjam/denda"),
						"method" => "POST",
						"data-id" => $id,
						// "enctype" => "multipart/form-data",
						// "target" => "_blank",
					]),
					"close" => Form::close(),

					"id" => Form::text("id",
						$id,
						["class" => "form-control id date d-none"]
					),
					"tanggal_kembali" => Form::text("tanggal_kembali",
						"",
						["class" => "form-control tanggal_kembali date"]
					),
					"denda" => Form::number("denda",
						"",
						["class" => "form-control denda"]
					),
				],
			);
			return view("$this->page.denda", $data);
		}
	}

	public function denda_proses (Request $r) {
		$id = $r->id;
		$sql = DB::table("pinjam")->where("id", $id)->get();
		if (count($sql) > 0) {
			$db = Pinjam::find($id);
			$db->tanggal_kembali = $r->tanggal_kembali;
			$db->save();

			$db = new Denda;
			$db->id_pinjam = $id;
			$db->denda = $r->denda;
			$db->save();

			$sql = DB::table("pinjam")->where("id", $id)->select("kode_buku")->first();
			DB::table("buku")->where("kode_buku", $sql->kode_buku)->update(["status" => "tersimpan"]);

			return redirect($this->module);
		}
	}

	public function rusak (Request $r) {
		$id = $r->id;
		$sql = DB::table("pinjam")->where("id", $id)->get();
		if (count($sql) > 0) {
			$data = array(
				"title" => "Rusak Pinjaman Buku",
				"back" => url($this->module),

				"form" => [
					"open" => Form::open([
						"class" => "form",
						"url" => url("pinjam/rusak"),
						"method" => "POST",
						"data-id" => $id,
						// "enctype" => "multipart/form-data",
						// "target" => "_blank",
					]),
					"close" => Form::close(),

					"id" => Form::text("id",
						$id,
						["class" => "form-control id date d-none"]
					),
					"tanggal_kembali" => Form::text("tanggal_kembali",
						"",
						["class" => "form-control tanggal_kembali date"]
					),
					"denda" => Form::number("denda",
						"",
						["class" => "form-control denda"]
					),
				],
			);
			return view("$this->page.denda", $data);
		}
	}

	public function rusak_proses (Request $r) {
		$id = $r->id;
		$sql = DB::table("pinjam")->where("id", $id)->get();
		if (count($sql) > 0) {
			$db = Pinjam::find($id);
			$db->tanggal_kembali = $r->tanggal_kembali;
			$db->save();

			$db = new Rusak;
			$db->id_pinjam = $id;
			$db->denda = $r->denda;
			$db->save();

			$sql = DB::table("pinjam")->where("id", $id)->select("kode_buku")->first();
			DB::table("buku")->where("kode_buku", $sql->kode_buku)->update(["status" => "rusak"]);

			return redirect($this->module);
		}
	}

	public function hilang (Request $r) {
		$id = $r->id;
		$sql = DB::table("pinjam")->where("id", $id)->get();
		if (count($sql) > 0) {
			$data = array(
				"title" => "Hilang Pinjaman Buku",
				"back" => url($this->module),

				"form" => [
					"open" => Form::open([
						"class" => "form",
						"url" => url("pinjam/hilang"),
						"method" => "POST",
						"data-id" => $id,
						// "enctype" => "multipart/form-data",
						// "target" => "_blank",
					]),
					"close" => Form::close(),

					"id" => Form::text("id",
						$id,
						["class" => "form-control id date d-none"]
					),
					"tanggal_kembali" => Form::text("tanggal_kembali",
						"",
						["class" => "form-control tanggal_kembali date"]
					),
					"denda" => Form::number("denda",
						"",
						["class" => "form-control denda"]
					),
				],
			);
			return view("$this->page.denda", $data);
		}
	}

	public function hilang_proses (Request $r) {
		$id = $r->id;
		$sql = DB::table("pinjam")->where("id", $id)->get();
		if (count($sql) > 0) {
			$db = Pinjam::find($id);
			$db->tanggal_kembali = $r->tanggal_kembali;
			$db->save();

			$db = new Hilang;
			$db->id_pinjam = $id;
			$db->denda = $r->denda;
			$db->save();

			$sql = DB::table("pinjam")->where("id", $id)->select("kode_buku")->first();
			DB::table("buku")->where("kode_buku", $sql->kode_buku)->update(["status" => "hilang"]);

			return redirect($this->module);
		}
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $db = Pinjam::find($id);
        $db->delete();
        return redirect($this->module);
	}

	protected function proses ($db, $r, $id = 0) {
        $db->kode_anggota = $r->kode_anggota;
        $db->kode_buku = $r->kode_buku;
        $db->tanggal_pinjam = "$r->tanggal_pinjam";
        $db->tanggal_harus_kembali = "$r->tanggal_harus_kembali";
        $db->tanggal_kembali = null;

		// update status buku
		if ($id > 0) {
			$sql = DB::table("pinjam")->where("id", $id)->select("kode_buku")->first();
			DB::table("buku")->where("kode_buku", $sql->kode_buku)->update(["status" => "tersimpan"]);
			DB::table("buku")->where("kode_buku", $r->kode_buku)->update(["status" => "pinjam"]);
		} else {
			DB::table("buku")->where("kode_buku", $r->kode_buku)->update(["status" => "pinjam"]);
		}

		$db->save();
	}

	protected function formData ($id = 0) {
		if ($id > 0) {
			$title = "Ubah Data $this->title";
			$aksi = url("$this->module/$id");
			$method = "PUT";

			$item = Pinjam::find($id);

			$sql = DB::table("pinjam as p")
				->where("p.id", "=", $id)
				->join("anggota as a", "p.kode_anggota", "a.kode_anggota")
				->join("buku as b", "p.kode_buku", "b.kode_buku")
				->select(
					"b.kode_buku",
					"judul_buku",
					"a.kode_anggota",
					"nama_anggota",
				)
				->get();

			if (count($sql) > 0) {
				$sql = $sql->first();
				$kode_buku = [$sql->kode_buku => "$sql->kode_buku $sql->judul_buku"];
				$kode_anggota = [$sql->kode_anggota => "$sql->kode_anggota $sql->nama_anggota"];
			}

		} else {
			$title = "Tambah Data $this->title";
			$method = "POST";
			$aksi = url($this->module);
		}

		return array(
            "title" => $title,
			"back" => url($this->module),

			"form" => [
				"open" => Form::open([
					"class" => "form",
					"url" => $aksi,
					"method" => $method,
					"data-id" => $id,
					// "enctype" => "multipart/form-data",
					// "target" => "_blank",
				]),
				"close" => Form::close(),

				"kode_buku" => Form::select("kode_buku",
					(empty($kode_buku) ? [] : $kode_buku),
					null,
					["class" => "form-control kode_buku"]
				),
				"kode_anggota" => Form::select("kode_anggota",
					(empty($kode_anggota) ? [] : $kode_anggota),
					null,
					["class" => "form-control kode_anggota"]
				),
				"tanggal_pinjam" => Form::text("tanggal_pinjam",
					(empty($item->tanggal_pinjam)
						? old("tanggal_pinjam")
						: $item->tanggal_pinjam),
					["class" => "form-control tanggal_pinjam date"]
				),
				"tanggal_harus_kembali" => Form::text("tanggal_harus_kembali",
					(empty($item->tanggal_harus_kembali)
						? old("tanggal_harus_kembali")
						: $item->tanggal_harus_kembali),
					["class" => "form-control tanggal_harus_kembali date"]
				),
			],
		);
	}

    protected function rules ($r) {
        $r->validate(
            array(
                "kode_buku" => 'required',
            ),
            array(
                "kode_buku.required" => "Kode buku tidak boleh kosong",
                // "kode_bidang.unique" => "Kode jenis telah digunakan",
                // "kode_bidang.min" => "Harap menggunakan minimal 5 karakter",
                // "kode_bidang.max" => "Harap menggunakan maksimal 10 karakter",
                // "nama_bidang.required" => "Nama jenis tidak boleh kosong",
            )
        );
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Kabupaten;
use Collective\Html\FormFacade as Form;

class KabupatenController extends Controller
{
    protected $page = "admin.page.kabupaten";
    protected $title = "Kabupaten";
    protected $module = "kabupaten";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r) {
		$perpage = 50;
		$page = empty($r->page) ? 1 : $r->page;
		$page = (($page*$perpage)-$perpage) + 1;

        $data = array(
            "title" => "Data $this->title",
            "form" => url("$this->module/create"),
			"module" => $this->module,
			"no" => $page,
			"data" => DB::table("kabupaten as k")
				->join("propinsi as p", "k.kode_propinsi", "p.kode_propinsi")
				->select(
					"k.id",
					"k.kode_kabupaten",
					"k.nama_kabupaten",
					"p.nama_propinsi",
				)
				->orderBy("id", "desc")
				->paginate($perpage)
        );

        return view("$this->page.data", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
		$data = $this->formData();
        return view("$this->page.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->rules($request);

		$db = new Kabupaten;
		$this->proses($db, $request);

        return redirect($this->module);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array(
            "title" => "Detail Data $this->title",
            "data" => Kabupaten::find($id)
        );
        return view("$this->page.detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$data = $this->formData($id);
        return view("$this->page.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules($request);

		$db = Kabupaten::find($id);
		$this->proses($db, $request);

        return redirect($this->module);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $db = Kabupaten::find($id);
        $db->delete();
        return redirect($this->module);
	}

	protected function proses ($db, $r) {
		if (!empty($r->kode_propinsi)) {
			$db->kode_propinsi = $r->kode_propinsi;
		}
        $db->kode_kabupaten = $r->kode_kabupaten;
		$db->nama_kabupaten = $r->nama_kabupaten;
        $db->save();
	}

	protected function formData ($id = 0) {
		if ($id > 0) {
			$title = "Ubah Data $this->title";
			$aksi = url("$this->module/$id");
			$method = "PUT";

			$item = Kabupaten::find($id);
			$kode_kabupaten = [$item->kode_kabupaten => $item->kode_kabupaten];

			$sql = DB::table("kabupaten as kab")
			->join("propinsi as pro", "kab.kode_propinsi", "pro.kode_propinsi")
			->select(
				"pro.kode_propinsi",
				"pro.nama_propinsi",
			)
			->where("kab.id", "=", $id)
			->first();

			$nama_propinsi = $sql->nama_propinsi;
			$kode_propinsi = $sql->kode_propinsi;

		} else {
			$title = "Tambah Data $this->title";
			$method = "POST";
			$aksi = url($this->module);
		}

		return array(
            "title" => $title,
			"back" => url($this->module),

			"nama_propinsi" => (empty($nama_propinsi) ? "" : $nama_propinsi),

			"form" => [
				"open" => Form::open([
					"class" => "form",
					"url" => $aksi,
					"method" => $method,
					"data-id" => $id,
					// "enctype" => "multipart/form-data",
					// "target" => "_blank",
				]),
				"close" => Form::close(),

				"kode_propinsi" => Form::select("kode_propinsi",
					[],
					null,
					["class" => "form-control kode_propinsi"]
				),

				"kode_kabupaten" => Form::select("kode_kabupaten",
					(empty($kode_kabupaten) ? [] : $kode_kabupaten),
					null,
					["class" => "form-control kode_kabupaten"]
				),
				"nama_kabupaten" => Form::text("nama_kabupaten",
					(empty($item->nama_kabupaten)
						? old("nama_kabupaten")
						: $item->nama_kabupaten),
					["class" => "form-control nama_kabupaten"]
				),
			],
		);
	}

    protected function rules ($r) {
        $r->validate(
            array(
                "kode_kabupaten" => 'required',
            ),
            array(
                "kode_kabupaten.required" => "Kode kabupaten tidak boleh kosong",
                // "kode_bidang.unique" => "Kode jenis telah digunakan",
                // "kode_bidang.min" => "Harap menggunakan minimal 5 karakter",
                // "kode_bidang.max" => "Harap menggunakan maksimal 10 karakter",
                // "nama_bidang.required" => "Nama jenis tidak boleh kosong",
            )
        );
    }
}

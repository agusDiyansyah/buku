<?php

namespace App\Http\Controllers\Part;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CheckerController extends Controller {
	function cekKode (Request $r) {
		if (
			$r->ajax()
			AND !empty($r->q)
			AND !empty($r->type)
		) {
			$cari = $r->q;
			$type = $r->type;
			$id = $r->id;

			DB::enableQueryLog();
			$sql = DB::table($type)
				->where("kode_$type", "=", "$cari")
				->where("id", "!=", $id)
				->get();
			// dd(DB::getQueryLog());

			$data = [];

			if (count($sql) > 0) {
				array_push($data, [
					"id" => "",
					"text" => "Kode telah digunakan",
					"disabled" => true,
				]);
			} else {
				array_push($data, [
					"id" => $cari,
					"text" => $cari,
					"disabled" => false,
				]);
			}

			return $data;
		}
	}

	function getKode (Request $r) {
		if (
			$r->ajax()
			AND !empty($r->q)
			AND !empty($r->type)
			AND !empty($r->title)
		) {
			$cari = $r->q;
			$type = $r->type;
			$title = $r->title;

			$sql = DB::table($type)
				->where("kode_$type", "=", "$cari")
				->orWhere($title, "like", "%$cari%")
				->select("kode_$type as id", "$title as text")
				->get();

			$data = [];

			if (count($sql) > 0) {
				foreach ($sql as $val) {
					array_push($data, [
						"id" => $val->id,
						"text" => "$val->id - $val->text",
						"disabled" => false,
					]);
				}
			} else {
				array_push($data, [
					"id" => " ",
					"text" => "Data tidak ditemukan",
					"disabled" => true,
				]);
			}

			return $data;
		}
	}
}

<?php

namespace App\Http\Controllers\Part;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class WilayahController extends Controller
{
    public function propinsi (Request $r) {
		if (
			$r->ajax()
		) {
			$stat = false;
			$data = [];
			$sql = DB::table("propinsi")->get();

			if (count($sql) > 0) {
				$stat = true;

				foreach ($sql as $val) {
					array_push($data, [
						"id" => $val->kode_propinsi,
						"text" => $val->nama_propinsi,
					]);
				}
			}

			return [
				"stat" => $stat,
				"data" => $data,
			];
		}
	}

	public function kabupaten (Request $r) {
		if (
			$r->ajax()
			AND !empty($r->kode_propinsi)
		) {
			$kode_propinsi = $r->kode_propinsi;
			$stat = false;
			$data = [];
			$sql = DB::table("kabupaten")->where("kode_propinsi", "=", $kode_propinsi)->get();

			if (count($sql) > 0) {
				$stat = true;

				foreach ($sql as $val) {
					array_push($data, [
						"id" => $val->kode_kabupaten,
						"text" => $val->nama_kabupaten,
					]);
				}
			}

			return [
				"stat" => $stat,
				"data" => $data,
			];
		}
	}

	public function kecamatan (Request $r) {
		if (
			$r->ajax()
			AND !empty($r->kode_kabupaten)
		) {
			$kode_kabupaten = $r->kode_kabupaten;
			$stat = false;
			$data = [];
			$sql = DB::table("kecamatan")->where("kode_kabupaten", "=", $kode_kabupaten)->get();

			if (count($sql) > 0) {
				$stat = true;

				foreach ($sql as $val) {
					array_push($data, [
						"id" => $val->kode_kecamatan,
						"text" => $val->nama_kecamatan,
					]);
				}
			}

			return [
				"stat" => $stat,
				"data" => $data,
			];
		}
	}

	// public function propinsi (Request $r) {
	// 	if (
	// 		$r->ajax()
	// 		AND !empty($r->q)
	// 	) {
	// 		$cari = $r->q;
	// 		$sql = DB::table("propinsi")->where("nama_propinsi", "like", "%$cari%")->get();
	// 		$data = [];

	// 		if (count($sql) > 0) {
	// 			foreach ($sql as $val) {
	// 				array_push($data, [
	// 					"id" => $val->kode_propinsi,
	// 					"text" => $val->nama_propinsi,
	// 					"disabled" => false,
	// 				]);
	// 			}
	// 		} else {
	// 			array_push($data, [
	// 				"id" => " ",
	// 				"text" => "Data tidak ditemukan",
	// 				"disabled" => false,
	// 			]);
	// 		}

	// 		return $data;
	// 	}
	// }

	// public function kabupaten (Request $r) {
	// 	if (
	// 		$r->ajax()
	// 		AND !empty($r->q)
	// 		AND !empty($r->kode_propinsi)
	// 	) {
	// 		$cari = $r->q;
	// 		$kode_propinsi = $r->kode_propinsi;
	// 		$sql = DB::table("kabupaten")
	// 			->where("nama_kabupaten", "like", "%$cari%")
	// 			->where("kode_propinsi", "=", $kode_propinsi)
	// 			->get();

	// 		$data = [];

	// 		if (count($sql) > 0) {
	// 			foreach ($sql as $val) {
	// 				array_push($data, [
	// 					"id" => $val->kode_kabupaten,
	// 					"text" => $val->nama_kabupaten,
	// 					"disabled" => false,
	// 				]);
	// 			}
	// 		} else {
	// 			array_push($data, [
	// 				"id" => " ",
	// 				"text" => "Data tidak ditemukan",
	// 				"disabled" => false,
	// 			]);
	// 		}

	// 		return $data;
	// 	}
	// }

	// public function kecamatan (Request $r) {
	// 	if (
	// 		$r->ajax()
	// 		AND !empty($r->q)
	// 		AND !empty($r->kode_kabupaten)
	// 	) {
	// 		$cari = $r->q;
	// 		$kode_kabupaten = $r->kode_kabupaten;
	// 		$sql = DB::table("kecamatan")
	// 			->where("nama_kecamatan", "like", "%$cari%")
	// 			->where("kode_kabupaten", "=", $kode_kabupaten)
	// 			->get();

	// 		$data = [];

	// 		if (count($sql) > 0) {
	// 			foreach ($sql as $val) {
	// 				array_push($data, [
	// 					"id" => $val->kode_kecamatan,
	// 					"text" => $val->nama_kecamatan,
	// 					"disabled" => false,
	// 				]);
	// 			}
	// 		} else {
	// 			array_push($data, [
	// 				"id" => " ",
	// 				"text" => "Data tidak ditemukan",
	// 				"disabled" => false,
	// 			]);
	// 		}

	// 		return $data;
	// 	}
	// }
}

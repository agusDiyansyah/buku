<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller {
	public function index () {
		return view("admin.login");
	}

	public function login (Request $r) {
		$username  = $r->username;
		$password  = $r->password;

		if ($username == "admin" AND $password == "1") {
			$r->session()->put("username", $username);
			$r->session()->put("logged", true);
			return redirect()->route('homeAdmin');
		} else {
			return redirect()->route('login');
		}
	}

	public function logout () {
		session()->flush();

		return redirect("/login");
	}
}

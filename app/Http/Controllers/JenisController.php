<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Jenis;

class JenisController extends Controller
{
    protected $page = "admin.page.jenis";
    protected $title = "Jenis Buku";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array(
            "title" => "Data $this->title",
            "data" => Jenis::orderBy('created_at', 'desc')->paginate(50)
        );

        return view("$this->page.data", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array(
            "title" => "Tambah Data $this->title",
            "method" => "POST",
            "aksi" => url("jenis"),
            "back" => url("jenis"),
            "data" => array(
                "kode_jenis" => old('kode_jenis'),
                "nama_jenis" => old('nama_jenis'),
            )
        );
        return view("$this->page.form", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
		$this->rules($request);

        $db = new Jenis;

        $db->kode_jenis = $request->kode_jenis;
        $db->nama_jenis = $request->nama_jenis;

        $db->save();
        return redirect('/jenis');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = array(
            "title" => "Detail Data $this->title",
            "data" => Jenis::find($id)
        );
        return view("$this->page.detail", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
		$item = Jenis::find($id);
        $data = array(
            "title" => "Ubah Data $this->title",
            "aksi" => url("jenis/$item->id"),
			"method" => "PUT",
			"back" => url("aksi"),
            "data" => array(
                "kode_jenis" => $item->kode_jenis,
                "nama_jenis" => $item->nama_jenis,
            )
        );
        return view("$this->page.form", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules($request);

        $db = Jenis::find($id);

        $db->kode_jenis = $request->kode_jenis;
        $db->nama_jenis = $request->nama_jenis;

        $db->save();

        return redirect('/jenis');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $db = Jenis::find($id);
        $db->delete();
        return redirect('/jenis');
    }

    protected function rules ($r) {
        $r->validate(
            array(
                "kode_jenis" => 'required|max:10|min:5',
                "nama_jenis" => 'required',
            ),
            array(
                "kode_jenis.required" => "Kode jenis tidak boleh kosong",
                // "kode_jenis.unique" => "Kode jenis telah digunakan",
                "kode_jenis.min" => "Harap menggunakan minimal 5 karakter",
                "kode_jenis.max" => "Harap menggunakan maksimal 10 karakter",
                "nama_jenis.required" => "Nama jenis tidak boleh kosong",
            )
        );
    }
}

@extends('admin.layout')

@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
			<span class="h2">{{ $title }}</span>
			<div class="btn-toolbar mb-2 mb-md-0">
				<div class="btn-group">
					<a href="{{ $back }}" class="btn btn-sm btn-outline-secondary">Kembali</a>
					{{-- <button type="button" class="btn btn-sm btn-outline-secondary">Laporan</button> --}}
				</div>
			</div>
		</div>

		{{ $form['open'] }}
			<div class="row">

				<div class="col-md-4">
					<div class="form-group">
						<label for="">Propinsi</label>
						{{ $form['kode_propinsi'] }}
						<b>{{ $nama_propinsi }}</b>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="">Kabupaten</label>
						{{ $form['kode_kabupaten'] }}
						<b>{{ $nama_kabupaten }}</b>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="">Kecamatan</label>
						{{ $form['kode_kecamatan'] }}
						<b>{{ $nama_kecamatan }}</b>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label for="kode_anggota">Kode Anggota</label>
						{{ $form['kode_anggota'] }}

						@if ($errors->has('kode_anggota'))
							<small class="text-danger"><em>{{ $errors->first('kode_anggota') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label for="nama_anggota">Nama Anggota</label>
						{{ $form['nama_anggota'] }}

						@if ($errors->has('nama_anggota'))
							<small class="text-danger"><em>{{ $errors->first('nama_anggota') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label for="alamat">Alamat</label>
						{{ $form['alamat'] }}

						@if ($errors->has('alamat'))
							<small class="text-danger"><em>{{ $errors->first('alamat') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="telp">Telp</label>
						{{ $form['telp'] }}

						@if ($errors->has('telp'))
							<small class="text-danger"><em>{{ $errors->first('telp') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="email">Email</label>
						{{ $form['email'] }}

						@if ($errors->has('email'))
							<small class="text-danger"><em>{{ $errors->first('email') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="tanggal_mulai_anggota">Tanggal Mulai Keanggotaan</label>
						{{ $form['tanggal_mulai_anggota'] }}

						@if ($errors->has('tanggal_mulai_anggota'))
							<small class="text-danger"><em>{{ $errors->first('tanggal_mulai_anggota') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="jenis_anggota">Jenis Anggota</label>
						{{ $form['jenis_anggota'] }}

						@if ($errors->has('jenis_anggota'))
							<small class="text-danger"><em>{{ $errors->first('jenis_anggota') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="status_anggota">Status Anggota</label>
						{{ $form['status_anggota'] }}

						@if ($errors->has('status_anggota'))
							<small class="text-danger"><em>{{ $errors->first('status_anggota') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm">Proses Data</button>
					</div>
				</div>

			</div>
		{{ $form['close'] }}
	</main>
@endsection

@section('script')
	<script>
		$(document).ready(function () {
			load_propinsi();

			$('.mn-anggota').addClass('active');

			$('.kode_anggota').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('cekKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							q : params.term,
							id : $('.form').data('id'),
							type : "anggota",
							_token : "{{ csrf_token() }}",
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});

			$('.kode_propinsi, .s2').select2({
				width: "100%"
			});

			$('.date').datepicker({
				autoclose: true,
				format: "yyyy-mm-dd"
			});

			$('.kode_propinsi').change(function (e) {
				e.preventDefault();
				var kode_propinsi = $(this).val();
				$.ajax({
					url: '{{ url('wilayah/kabupaten') }}',
					type: 'post',
					dataType: 'json',
					data: {
						_token : "{{ csrf_token() }}",
						kode_propinsi: kode_propinsi
					},
					beforeSend: function () {
						if ($('.kode_kabupaten').hasClass("select2-hidden-accessible")) {
							$('.kode_kabupaten').select2('destroy');
						}
					},
					success: function (json) {
						var opt = "";
						if (json.stat) {
							opt = "<option value=''></option>";
							json.data.forEach(e => {
								opt += "<option value='"+ e.id +"'>"+ e.text +"</option>";
							});
						}

						$('.kode_kabupaten').html(opt);
						$('.kode_kabupaten').select2({
							width: "100%"
						});
					}
				});
			});

			$('.kode_kabupaten').change(function (e) {
				e.preventDefault();
				var kode_kabupaten = $(this).val();
				$.ajax({
					url: '{{ url('wilayah/kecamatan') }}',
					type: 'post',
					dataType: 'json',
					data: {
						_token : "{{ csrf_token() }}",
						kode_kabupaten: kode_kabupaten
					},
					beforeSend: function () {
						if ($('.kode_kecamatan').hasClass("select2-hidden-accessible")) {
							$('.kode_kecamatan').select2('destroy');
						}
					},
					success: function (json) {
						var opt = "";
						if (json.stat) {
							opt = "<option value=''></option>";
							json.data.forEach(e => {
								opt += "<option value='"+ e.id +"'>"+ e.text +"</option>";
							});
						}

						$('.kode_kecamatan').html(opt);
						$('.kode_kecamatan').select2({
							width: "100%"
						});
					}
				});
			});

			function load_propinsi () {
				$.ajax({
					url: '{{ url('wilayah/propinsi') }}',
					type: 'post',
					dataType: 'json',
					data: {
						_token : "{{ csrf_token() }}",
					},
					beforeSend: function () {
						if ($('.kode_propinsi').hasClass("select2-hidden-accessible")) {
							$('.kode_propinsi').select2('destroy');
						}
					},
					success: function (json) {
						var opt = "";
						if (json.stat) {
							opt = "<option value=''></option>";
							json.data.forEach(e => {
								opt += "<option value='"+ e.id +"'>"+ e.text +"</option>";
							});
						}

						$('.kode_propinsi').html(opt);
						$('.kode_propinsi').select2({
							width: "100%"
						});
					}
				});
			}
		});
	</script>
@endsection

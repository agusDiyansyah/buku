@extends('admin.layout')

@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
			<span class="h2">{{ $title }}</span>
			<div class="btn-toolbar mb-2 mb-md-0">
				<div class="btn-group">
					<a href="{{ $back }}" class="btn btn-sm btn-outline-secondary">Kembali</a>
					{{-- <button type="button" class="btn btn-sm btn-outline-secondary">Laporan</button> --}}
				</div>
			</div>
		</div>

		{{ $form['open'] }}
			<div class="row">

				<div class="col-md-4">
					<div class="form-group">
						<label for="">Propinsi</label>
						{{ $form['kode_propinsi'] }}
						<b>{{ $nama_propinsi }}</b>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="">Kabupaten</label>
						{{ $form['kode_kabupaten'] }}
						<b>{{ $nama_kabupaten }}</b>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="">Kecamatan</label>
						{{ $form['kode_kecamatan'] }}
						<b>{{ $nama_kecamatan }}</b>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label for="kode_penulis">Kode Penulis</label>
						{{ $form['kode_penulis'] }}

						@if ($errors->has('kode_penulis'))
							<small class="text-danger"><em>{{ $errors->first('kode_penulis') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label for="nama_penulis_utama">Nama Penulis Utama</label>
						{{ $form['nama_penulis_utama'] }}

						@if ($errors->has('nama_penulis_utama'))
							<small class="text-danger"><em>{{ $errors->first('nama_penulis_utama') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label for="alamat">Alamat</label>
						{{ $form['alamat'] }}

						@if ($errors->has('alamat'))
							<small class="text-danger"><em>{{ $errors->first('alamat') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="telp">Telp</label>
						{{ $form['telp'] }}

						@if ($errors->has('telp'))
							<small class="text-danger"><em>{{ $errors->first('telp') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="email">Email</label>
						{{ $form['email'] }}

						@if ($errors->has('email'))
							<small class="text-danger"><em>{{ $errors->first('email') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm">Proses Data</button>
					</div>
				</div>

			</div>
		{{ $form['close'] }}
	</main>
@endsection

@section('script')
	<script>
		$(document).ready(function () {
			$('.mn-penulis').addClass('active');

			$('.kode_penulis').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('cekKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							_token : "{{ csrf_token() }}",
							q : params.term,
							type : "penulis",
							id: $('.form').data('id'),
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});

			load_propinsi();
			$('.kode_propinsi').select2({
				width: "100%"
			});

			$('.kode_propinsi').change(function (e) {
				e.preventDefault();
				var kode_propinsi = $(this).val();
				$.ajax({
					url: '{{ url('wilayah/kabupaten') }}',
					type: 'post',
					dataType: 'json',
					data: {
						_token : "{{ csrf_token() }}",
						kode_propinsi: kode_propinsi
					},
					beforeSend: function () {
						if ($('.kode_kabupaten').hasClass("select2-hidden-accessible")) {
							$('.kode_kabupaten').select2('destroy');
						}
					},
					success: function (json) {
						var opt = "";
						if (json.stat) {
							opt = "<option value=''></option>";
							json.data.forEach(e => {
								opt += "<option value='"+ e.id +"'>"+ e.text +"</option>";
							});
						}

						$('.kode_kabupaten').html(opt);
						$('.kode_kabupaten').select2({
							width: "100%"
						});
					}
				});
			});

			$('.kode_kabupaten').change(function (e) {
				e.preventDefault();
				var kode_kabupaten = $(this).val();
				$.ajax({
					url: '{{ url('wilayah/kecamatan') }}',
					type: 'post',
					dataType: 'json',
					data: {
						_token : "{{ csrf_token() }}",
						kode_kabupaten: kode_kabupaten
					},
					beforeSend: function () {
						if ($('.kode_kecamatan').hasClass("select2-hidden-accessible")) {
							$('.kode_kecamatan').select2('destroy');
						}
					},
					success: function (json) {
						var opt = "";
						if (json.stat) {
							opt = "<option value=''></option>";
							json.data.forEach(e => {
								opt += "<option value='"+ e.id +"'>"+ e.text +"</option>";
							});
						}

						$('.kode_kecamatan').html(opt);
						$('.kode_kecamatan').select2({
							width: "100%"
						});
					}
				});
			});

			function load_propinsi () {
				$.ajax({
					url: '{{ url('wilayah/propinsi') }}',
					type: 'post',
					dataType: 'json',
					data: {
						_token : "{{ csrf_token() }}",
					},
					beforeSend: function () {
						if ($('.kode_propinsi').hasClass("select2-hidden-accessible")) {
							$('.kode_propinsi').select2('destroy');
						}
					},
					success: function (json) {
						var opt = "";
						if (json.stat) {
							opt = "<option value=''></option>";
							json.data.forEach(e => {
								opt += "<option value='"+ e.id +"'>"+ e.text +"</option>";
							});
						}

						$('.kode_propinsi').html(opt);
						$('.kode_propinsi').select2({
							width: "100%"
						});
					}
				});
			}

			// $('.kode_kabupaten').select2({
			// 	placeholder        : '',
			// 	minimumInputLength : 2,
			// 	width: '100%',
			// 	ajax: {
			// 		url      : '{{ url("/wilayah/kabupaten") }}',
			// 		dataType : 'json',
			// 		type     : 'POST',
			// 		delay    : 250,
			// 		data: function (params) {
			// 			return {
			// 				_token : "{{ csrf_token() }}",
			// 				q : params.term,
			// 				kode_propinsi : $('.kode_propinsi').val(),
			// 			};
			// 		},
			// 		processResults: function (json) {
			// 			return {
			// 				results: json
			// 			};
			// 		},
			// 		cache: false,
			// 	}
			// });

			// $('.kode_kecamatan').select2({
			// 	placeholder        : '',
			// 	minimumInputLength : 2,
			// 	width: '100%',
			// 	ajax: {
			// 		url      : '{{ url("/wilayah/kecamatan") }}',
			// 		dataType : 'json',
			// 		type     : 'POST',
			// 		delay    : 250,
			// 		data: function (params) {
			// 			return {
			// 				_token : "{{ csrf_token() }}",
			// 				q : params.term,
			// 				kode_kabupaten : $('.kode_kabupaten').val(),
			// 			};
			// 		},
			// 		processResults: function (json) {
			// 			return {
			// 				results: json
			// 			};
			// 		},
			// 		cache: false,
			// 	}
			// });
		});
	</script>
@endsection

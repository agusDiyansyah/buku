@extends('admin.layout')

@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
			<span class="h2">Dashboard</span>
			<div class="btn-toolbar mb-2 mb-md-0">
				<div class="btn-group mr-2">
					<button type="button" class="btn btn-sm btn-outline-secondary">Tambah Data</button>
					<button type="button" class="btn btn-sm btn-outline-secondary">Laporan</button>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo magnam doloribus cumque ea ab, vel architecto maiores debitis eligendi! Pariatur error eaque architecto iusto fugit laborum ad nulla eos velit!
			</div>
		</div>
	</main>
@endsection

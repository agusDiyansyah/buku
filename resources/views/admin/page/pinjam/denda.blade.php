@extends('admin.layout')

@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
			<span class="h2">{{ $title }}</span>
			<div class="btn-toolbar mb-2 mb-md-0">
				<div class="btn-group">
					<a href="{{ $back }}" class="btn btn-sm btn-outline-secondary">Kembali</a>
					{{-- <button type="button" class="btn btn-sm btn-outline-secondary">Laporan</button> --}}
				</div>
			</div>
		</div>

		{{ $form['open'] }}
		{{ $form['id'] }}
			<div class="row">

				<div class="col-md-6">
					<div class="form-group">
						<label for="tanggal_kembali">Tanggal Kembali</label>
						{{ $form['tanggal_kembali'] }}

						@if ($errors->has('tanggal_kembali'))
							<small class="text-danger"><em>{{ $errors->first('tanggal_kembali') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
						<div class="form-group">
							<label for="denda">Denda</label>
							{{ $form['denda'] }}

							@if ($errors->has('denda'))
								<small class="text-danger"><em>{{ $errors->first('denda') }}</em></small>
							@endif
						</div>
					</div>

				<div class="col-md-12">
					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm">Proses Data</button>
					</div>
				</div>

			</div>
		{{ $form['close'] }}
	</main>
@endsection

@section('script')
	<script>
		$(document).ready(function () {
			$('.mn-pinjam').addClass('active');

			$('.date').datepicker({
				autoclose: true,
				format: "yyyy-mm-dd"
			});
		});
	</script>
@endsection

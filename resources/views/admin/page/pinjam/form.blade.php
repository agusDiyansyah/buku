@extends('admin.layout')

@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
			<span class="h2">{{ $title }}</span>
			<div class="btn-toolbar mb-2 mb-md-0">
				<div class="btn-group">
					<a href="{{ $back }}" class="btn btn-sm btn-outline-secondary">Kembali</a>
					{{-- <button type="button" class="btn btn-sm btn-outline-secondary">Laporan</button> --}}
				</div>
			</div>
		</div>

		{{ $form['open'] }}
			<div class="row">

				<div class="col-md-12">
					<div class="form-group">
						<label for="kode_buku">Judul / Kode Buku</label>
						{{ $form['kode_buku'] }}

						@if ($errors->has('kode_buku'))
							<small class="text-danger"><em>{{ $errors->first('kode_buku') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label for="kode_anggota">Nama / Kode Anggota</label>
						{{ $form['kode_anggota'] }}

						@if ($errors->has('kode_anggota'))
							<small class="text-danger"><em>{{ $errors->first('kode_anggota') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="tanggal_pinjam">Tanggal Pinjam</label>
						{{ $form['tanggal_pinjam'] }}

						@if ($errors->has('tanggal_pinjam'))
							<small class="text-danger"><em>{{ $errors->first('tanggal_pinjam') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="tanggal_harus_kembali">Tanggal Harus Kembali</label>
						{{ $form['tanggal_harus_kembali'] }}

						@if ($errors->has('tanggal_harus_kembali'))
							<small class="text-danger"><em>{{ $errors->first('tanggal_harus_kembali') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm">Proses Data</button>
					</div>
				</div>

			</div>
		{{ $form['close'] }}
	</main>
@endsection

@section('script')
	<script>
		$(document).ready(function () {
			$('.mn-pinjam').addClass('active');

			$('.s2').select2({
				width: "100%"
			});

			$('.date').datepicker({
				autoclose: true,
				format: "yyyy-mm-dd"
			});

			$('.kode_buku').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('getKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							q : params.term,
							type : "buku",
							title : "judul_buku",
							_token : "{{ csrf_token() }}",
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});

			$('.kode_anggota').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('getKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							q : params.term,
							type : "anggota",
							title : "nama_anggota",
							_token : "{{ csrf_token() }}",
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});
		});
	</script>
@endsection

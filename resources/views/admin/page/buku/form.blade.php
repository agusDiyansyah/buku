@extends('admin.layout')

@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
			<span class="h2">{{ $title }}</span>
			<div class="btn-toolbar mb-2 mb-md-0">
				<div class="btn-group">
					<a href="{{ $back }}" class="btn btn-sm btn-outline-secondary">Kembali</a>
					{{-- <button type="button" class="btn btn-sm btn-outline-secondary">Laporan</button> --}}
				</div>
			</div>
		</div>

		{{ $form['open'] }}
			<div class="row">

				<div class="col-md-12">
					<div class="form-group">
						<label for="kode_buku">Kode Buku</label>
						{{ $form['kode_buku'] }}

						@if ($errors->has('kode_buku'))
							<small class="text-danger"><em>{{ $errors->first('kode_buku') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
						<label for="judul_buku">Judul Buku</label>
						{{ $form['judul_buku'] }}

						@if ($errors->has('judul_buku'))
							<small class="text-danger"><em>{{ $errors->first('judul_buku') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="kode_jenis">Kode Jenis</label>
						{{ $form['kode_jenis'] }}

						@if ($errors->has('kode_jenis'))
							<small class="text-danger"><em>{{ $errors->first('kode_jenis') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="kode_bidang">Kode Bidang</label>
						{{ $form['kode_bidang'] }}

						@if ($errors->has('kode_bidang'))
							<small class="text-danger"><em>{{ $errors->first('kode_bidang') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="kode_penulis">Kode Penulis</label>
						{{ $form['kode_penulis'] }}

						@if ($errors->has('kode_penulis'))
							<small class="text-danger"><em>{{ $errors->first('kode_penulis') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="kode_penerbit">Kode Penerbit</label>
						{{ $form['kode_penerbit'] }}

						@if ($errors->has('kode_penerbit'))
							<small class="text-danger"><em>{{ $errors->first('kode_penerbit') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="jumlah">Jumlah</label>
						{{ $form['jumlah'] }}

						@if ($errors->has('jumlah'))
							<small class="text-danger"><em>{{ $errors->first('jumlah') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="bahasa">Bahasa</label>
						{{ $form['bahasa'] }}

						@if ($errors->has('bahasa'))
							<small class="text-danger"><em>{{ $errors->first('bahasa') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="isbn">ISBN</label>
						{{ $form['isbn'] }}

						@if ($errors->has('isbn'))
							<small class="text-danger"><em>{{ $errors->first('isbn') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label for="tahun">Tahun</label>
						{{ $form['tahun'] }}

						@if ($errors->has('tahun'))
							<small class="text-danger"><em>{{ $errors->first('tahun') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="jumlah_halaman">Jumlah Halaman</label>
						{{ $form['jumlah_halaman'] }}

						@if ($errors->has('jumlah_halaman'))
							<small class="text-danger"><em>{{ $errors->first('jumlah_halaman') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="edisi">Edisi</label>
						{{ $form['edisi'] }}

						@if ($errors->has('edisi'))
							<small class="text-danger"><em>{{ $errors->first('edisi') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label for="cetakan_ke">Cetakan Ke</label>
						{{ $form['cetakan_ke'] }}

						@if ($errors->has('cetakan_ke'))
							<small class="text-danger"><em>{{ $errors->first('cetakan_ke') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm">Proses Data</button>
					</div>
				</div>

			</div>
		{{ $form['close'] }}
	</main>
@endsection

@section('script')
	<script>
		$(document).ready(function () {
			$('.mn-buku').addClass('active');

			$('.s2').select2({
				width: "100%"
			});

			$('.tahun').datepicker({
				autoclose: true,
				minViewMode: 2,
				maxViewMode: 2,
				format: "yyyy"
			});

			$('.kode_buku').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('cekKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							q : params.term,
							id : $('.form').data('id'),
							type : "buku",
							_token : "{{ csrf_token() }}",
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});

			$('.kode_jenis').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('getKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							q : params.term,
							type : "jenis",
							title : "nama_jenis",
							_token : "{{ csrf_token() }}",
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});

			$('.kode_bidang').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('getKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							q : params.term,
							type : "bidang",
							title : "nama_bidang",
							_token : "{{ csrf_token() }}",
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});

			$('.kode_penulis').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('getKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							q : params.term,
							type : "penulis",
							title : "nama_penulis_utama",
							_token : "{{ csrf_token() }}",
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});

			$('.kode_penerbit').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('getKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							q : params.term,
							type : "penerbit",
							title : "nama_penerbit",
							_token : "{{ csrf_token() }}",
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});
		});
	</script>
@endsection

@extends('admin.layout')

@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
			<span class="h2">{{ $title }}</span>
			<div class="btn-toolbar mb-2 mb-md-0">
				<div class="btn-group mr-2">
					<a href="{{ $form }}" class="btn btn-sm btn-outline-secondary">Tambah Data</a>
					{{-- <button type="button" class="btn btn-sm btn-outline-secondary">Laporan</button> --}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th width="5%">#</th>
							<th>Aksi</th>
							<th>Nama Propinsi</th>
							<th>Nama Kabupaten</th>
							<th>Kode Kabupaten</th>
							<th>Nama Kabupaten</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($data as $item)
							<tr>
								<td>{{ $no++ }}</td>
								<td>
									{{-- <a href="{{ url("bukutamu/$item->id") }}" style="min-width: 35px" class="btn btn-sm btn-secondary">
										<i class="fa fa-list"></i>
									</a> --}}
									<a href="{{ url("$module/$item->id/edit") }}" style="min-width: 35px" class="btn btn-sm btn-primary">
										<i class="fa fa-pencil"></i>
									</a>
									<a href="#" style="min-width: 35px" data-id="{{ $item->id }}" class="btn btn-delete btn-sm btn-danger">
										<i class="fa fa-trash"></i>
									</a>
								</td>
								<td>{{ $item->nama_propinsi }}</td>
								<td>{{ $item->nama_kabupaten }}</td>
								<td>{{ $item->kode_kecamatan }}</td>
								<td>{{ $item->nama_kecamatan }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>

				{{ $data->links() }}
			</div>
		</div>
	</main>

	<form action="" method="POST" class="form-delete">@csrf @method("DELETE")</form>
@endsection

@section('script')
	<script>
		$(document).ready(function () {
			$('.mn-kecamatan').addClass('active');

			$('.table').find('.btn-delete').on('click', function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                hapus(id, $(this));
            })

            function hapus(id, o) {
                var m = $('.modal');
                var data = "Anda yakin akan menghapus data ini";

                m.find('.modal-title').html('Hapus Data');
                m.find('.modal-body').html(data);
                m.modal('show');
                m.find('.btn-aksi').click(function (e) {
                    e.preventDefault();
                    $('.form-delete').attr('action', '{{url('kecamatan')}}/' + id);
                    $('.form-delete').submit();
                });
            }
		});
	</script>
@endsection

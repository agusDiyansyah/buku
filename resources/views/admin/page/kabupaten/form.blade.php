@extends('admin.layout')

@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
			<span class="h2">{{ $title }}</span>
			<div class="btn-toolbar mb-2 mb-md-0">
				<div class="btn-group">
					<a href="{{ $back }}" class="btn btn-sm btn-outline-secondary">Kembali</a>
					{{-- <button type="button" class="btn btn-sm btn-outline-secondary">Laporan</button> --}}
				</div>
			</div>
		</div>

		{{ $form['open'] }}
			<div class="row">

				<div class="col-md-12">
					<div class="form-group">
						<label for="">Propinsi</label>
						{{ $form['kode_propinsi'] }}
						<b>{{ $nama_propinsi }}</b>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="kode_kabupaten">Kode Kabupaten</label>
						{{ $form['kode_kabupaten'] }}

						@if ($errors->has('kode_kabupaten'))
							<small class="text-danger"><em>{{ $errors->first('kode_kabupaten') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label for="nama_kabupaten">Nama Kabupaten</label>
						{{ $form['nama_kabupaten'] }}

						@if ($errors->has('nama_kabupaten'))
							<small class="text-danger"><em>{{ $errors->first('nama_kabupaten') }}</em></small>
						@endif
					</div>
				</div>

				<div class="col-md-12">
					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm">Proses Data</button>
					</div>
				</div>

			</div>
		{{ $form['close'] }}
	</main>
@endsection

@section('script')
	<script>
		$(document).ready(function () {
			load_propinsi();

			$('.mn-kabupaten').addClass('active');

			$('.kode_kabupaten').select2({
				placeholder        : '',
				minimumInputLength : 2,
				width: '100%',
				ajax: {
					url      : '{{ URL::route('cekKode') }}',
					dataType : 'json',
					type     : 'POST',
					delay    : 250,
					data: function (params) {
						return {
							q : params.term,
							id : $('.form').data('id'),
							type : "kabupaten",
							_token : "{{ csrf_token() }}",
						};
					},
					processResults: function (json) {
						return {
							results: json
						};
					},
					cache: false,
				}
			});

			$('.kode_propinsi').select2({
				width: "100%"
			});

			function load_propinsi () {
				$.ajax({
					url: '{{ url('wilayah/propinsi') }}',
					type: 'post',
					dataType: 'json',
					data: {
						_token : "{{ csrf_token() }}",
					},
					beforeSend: function () {
						if ($('.kode_propinsi').hasClass("select2-hidden-accessible")) {
							$('.kode_propinsi').select2('destroy');
						}
					},
					success: function (json) {
						var opt = "";
						if (json.stat) {
							opt = "<option value=''></option>";
							json.data.forEach(e => {
								opt += "<option value='"+ e.id +"'>"+ e.text +"</option>";
							});
						}

						$('.kode_propinsi').html(opt);
						$('.kode_propinsi').select2({
							width: "100%"
						});
					}
				});
			}
		});
	</script>
@endsection

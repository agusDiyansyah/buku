@extends('admin.layout')

@section('content')
	<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
			<span class="h2">{{ $title }}</span>
			<div class="btn-toolbar mb-2 mb-md-0">
				<div class="btn-group mr-2">
					<a href="{{ $back }}" class="btn btn-sm btn-outline-secondary">Kembali</a>
					{{-- <button type="button" class="btn btn-sm btn-outline-secondary">Laporan</button> --}}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<form action="{{ $aksi }}" method="post">
					@csrf
					@method($method)

					<div class="form-group">
						<label for="kode_bidang">Kode Bidang</label>
						<input type="text" class="form-control" name="kode_bidang" value="{{ @$data['kode_bidang'] }}">

						@if ($errors->has('kode_bidang'))
							<small class="text-danger"><em>{{ $errors->first('kode_bidang') }}</em></small>
						@endif
					</div>

					<div class="form-group">
						<label for="nama_bidang">Bidang</label>
						<input type="nama_bidang" class="form-control" name="nama_bidang" value="{{ @$data['nama_bidang'] }}">

						@if ($errors->has('nama_bidang'))
							<small class="text-danger"><em>{{ $errors->first('nama_bidang') }}</em></small>
						@endif
					</div>

					<div class="text-right">
						<button type="submit" class="btn btn-primary btn-sm">Proses Data</button>
					</div>
				</form>
			</div>
		</div>
	</main>
@endsection

@section('script')
	<script>
		$(document).ready(function () {
			$('.mn-bidang').addClass('active');
		});
	</script>
@endsection

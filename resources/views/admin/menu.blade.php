<nav class="col-md-2 d-none d-md-block bg-light sidebar">
	<div class="sidebar-sticky">
		<ul class="nav flex-column">
			<li class="nav-item">
				<a class="nav-link active" href="#">
					Dashboard
					<i class="fa fa-home float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link mn-pinjam" href="{{ url("/pinjam") }}">
					Pinjam Buku
					<i class="fa fa-book float-right"></i>
				</a>
			</li>
		</ul>

		<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			<span>Data Buku</span>
			<a class="d-flex align-items-center text-muted" href="#">
				<span data-feather="plus-circle"></span>
			</a>
		</h6>
		<ul class="nav flex-column">
			<li class="nav-item">
				<a class="nav-link mn-buku" href="{{ url("buku") }}">
					Buku
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link mn-penulis" href="{{ url("penulis") }}">
					Penulis
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link mn-penerbit" href="{{ url("penerbit") }}">
					Penerbit
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link mn-jenis" href="{{ url("jenis") }}">
					Jenis Buku
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link mn-bidang" href="{{ url("bidang") }}">
					Bidang
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
		</ul>

		<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			<span>Data Anggota</span>
			<a class="d-flex align-items-center text-muted" href="#">
				<span data-feather="plus-circle"></span>
			</a>
		</h6>
		<ul class="nav flex-column">
			<li class="nav-item">
				<a class="nav-link mn-anggota" href="{{ url("/anggota") }}">
					Anggota
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link mn-propinsi" href="{{ url("/propinsi") }}">
					Propinsi
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link mn-kabupaten" href="{{ url("/kabupaten") }}">
					Kabupaten
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link mn-kecamatan" href="{{ url("/kecamatan") }}">
					Kecamatan
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
		</ul>

		{{-- <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			<span>Web Setting</span>
			<a class="d-flex align-items-center text-muted" href="#">
				<span data-feather="plus-circle"></span>
			</a>
		</h6>
		<ul class="nav flex-column">
			<li class="nav-item">
				<a class="nav-link" href="#">
					Tanggal Libur
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">
					Tarif Denda
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">
					Tarif Hilang
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">
					Tarif Rusak
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
		</ul>

		<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			<span>Administrator</span>
			<a class="d-flex align-items-center text-muted" href="#">
				<span data-feather="plus-circle"></span>
			</a>
		</h6>
		<ul class="nav flex-column">
			<li class="nav-item">
				<a class="nav-link" href="#">
					User
					<i class="fa fa-th-large float-right"></i>
				</a>
			</li>
		</ul> --}}
	</div>
</nav>

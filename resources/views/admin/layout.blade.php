<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Buku</title>

		<link rel="stylesheet" href="{{ asset("assets/admin/vendor/bootstrap/css/bootstrap.min.css") }}">
		<link rel="stylesheet" href="{{ asset("assets/admin/vendor/font-awesome/css/font-awesome.min.css") }}">
		<link rel="stylesheet" href="{{ asset("assets/admin/vendor/select2/select2.css") }}">
		<link rel="stylesheet" href="{{ asset("assets/admin/vendor/select2/custom-select2.css") }}">
		<link rel="stylesheet" href="{{ asset("assets/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css") }}">
		<link rel="stylesheet" href="{{ asset("assets/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css") }}">

		<!-- Custom styles for this template -->
		<link href="{{ asset("assets/admin/css/custom.css") }}" rel="stylesheet">

		@yield('css')
	</head>
	<body>
		<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
			<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Database Buku</a>
			{{-- <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"> --}}
			<ul class="navbar-nav px-3">
				<li class="nav-item text-nowrap">
					<a class="nav-link" href="{{ url("logout") }}">Sign out</a>
				</li>
			</ul>
		</nav>
		<div class="container-fluid">
			<div class="row">
				@include('admin.menu')

				@yield('content')
			</div>
		</div>

		<div class="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Modal body text goes here.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
					<button type="button" class="btn btn-primary btn-aksi">Proses</button>
				</div>
				</div>
			</div>
		</div>

		<script src="{{ asset("assets/admin/vendor/jquery/jquery.min.js") }}"></script>
		<script src="{{ asset("assets/admin/vendor/bootstrap/js/popper.js") }}"></script>
		<script src="{{ asset("assets/admin/vendor/bootstrap/js/bootstrap.min.js") }}"></script>
		<script src="{{ asset("assets/admin/vendor/select2/select2.js") }}"></script>
		<script src="{{ asset("assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}"></script>

		<script src="{{ asset("assets/admin/js/custom.js") }}"></script>

		@yield('script')
	</body>
</html>

<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Login</title>

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="assets/admin/vendor/bootstrap/css/bootstrap.min.css">


		<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
			font-size: 3.5rem;
			}
		}

		</style>
		<!-- Custom styles for this template -->
		<link href="{{ asset("assets/admin/css/login.css") }}" rel="stylesheet">
	</head>
	<body class="text-center">
		<form class="form-signin" method="post">
			@csrf
			@method("post")

			<b class="mb-3 h4 d-block font-weight-normal text-left">Data Buku</b>
			<span class="mb-3 d-block font-weight-normal text-left">Silahkan menginputkan username dan password yang telah diberikan</span>

			<label for="username" class="sr-only">Username</label>
			<input type="text" id="username" name="username" class="form-control mb-2" placeholder="Username" required autofocus>

			<label for="password" class="sr-only">Password</label>
			<input type="password" id="password" name="password" class="form-control" placeholder="Password" required>

			<button class="btn btn-lg btn-primary btn-block" type="submit">Masuk</button>
		</form>
	</body>
</html>
